package main

import (
	"context"
	"fmt"
	"gitlabapi"
	"log"
	"net/http"
	"os"
	"time"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

//							Bad request handler
//-----------------------------------------------------------------------------

//  Handles mistyped url's and sends an error
func handlerBadRequest(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Bad Request", http.StatusBadRequest)
}

//								Main
//-----------------------------------------------------------------------------

func main() {

	gitlabapi.Uptime = time.Now() // Keeps track of time at start

	//						Firestore initialisation
	//-------------------------------------------------------------------------

	gitlabapi.Ctx = context.Background()

	sa := option.WithCredentialsFile(gitlabapi.AdminSDK) // Setting service account
	app, err := firebase.NewApp(gitlabapi.Ctx, nil, sa)  // Creating new app

	if err != nil {
		log.Fatalln(err)
	}

	gitlabapi.Client, err = app.Firestore(gitlabapi.Ctx) // Setting the client
	if err != nil {
		log.Fatalln(err)
	}

	gitlabapi.GetData() // Fetching webhooks from database and store it locally

	defer gitlabapi.Client.Close() // Closing client at end

	//	Gets port and checks if it's "8080", if not port is set to be "8080"
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("Port not found! Setting to 8080")
	}

	//  HandleFunc() for each endpoint
	http.HandleFunc("/", handlerBadRequest)
	http.HandleFunc(gitlabapi.CommitsRootPath, gitlabapi.HandlerCommits)
	http.HandleFunc(gitlabapi.IssuesRootPath, gitlabapi.HandlerIssues)
	http.HandleFunc(gitlabapi.StatusRootPath, gitlabapi.HandlerStatus)
	http.HandleFunc(gitlabapi.WebhooksRootPath1, gitlabapi.HandlerWebhooks)
	http.HandleFunc(gitlabapi.WebhooksRootPath2, gitlabapi.HandlerWebhooks)

	fmt.Println("Listening on port " + port) //  Prints which port server is listening on

	log.Fatal(http.ListenAndServe(":"+port, nil)) //  Initialises server
}
