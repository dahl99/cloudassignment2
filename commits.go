package gitlabapi

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

//									Structs
//-----------------------------------------------------------------------------

//Repository stores the finalized data about each repo
type Repository struct {
	Path  string `json:"path_with_namespace"`
	Count int    `json:"commits"`
}

//Commits store each commit so they can be counted
type Commits struct {
	ID string `json:"id"`
}

//AllRepos contains the finalized data that will be displayed
type AllRepos struct {
	Repos []Repository `json:"repositories"`
	Auth  bool         `json:"auth"`
}

//									Handler
//-----------------------------------------------------------------------------

//HandlerCommits is the handler for the commits endpoint
func HandlerCommits(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") // Splits url into parts divided by '/'

	if len(parts) < 4 { // Checks if correct path was entered
		http.Error(w, "Bad url", http.StatusBadRequest)
		log.Println(http.StatusBadRequest)
		return
	}

	limitString := r.FormValue("limit") // Gets the value of limit from request

	if limitString == "" { //	Checks if limit is empty, if so it's set to 5
		limitString = "5"
	} else if limitString < "1" || limitString > "9" { // If limit is invalid it's set to 5 instead
		limitString = "5"
	}

	limit, err := strconv.Atoi(limitString) // Converts limit from string to int
	if err != nil {
		http.Error(w, "Limit from string to int failed", http.StatusNoContent)
		log.Println(http.StatusNoContent)
		return
	}

	token := r.FormValue("auth") //	Gets the auth token if used
	sourceAll := getRepos(w, r)  // Fetching the repositories

	//								Fetching commits
	//-----------------------------------------------------------------------------

	pageCount := 1 // Variable used to keep track of current page

	for i := 0; i < len(sourceAll); i++ {

		// Sets base url root path for each page
		baseURL := GitlabRootPath + "/" + strconv.Itoa(sourceAll[i].ID) + "/repository/commits?"

		if token != "" { // Adds token if used
			baseURL += "private_token=" + token + "&"
		}

		baseURL += "per_page=100&page=" // Adds per_page and page because page needs to be last in url
		pageCount = 1                   // Resets page number

		// Loops through all pages with commits and stops when page has <100 commits
		for whileBool := true; whileBool; pageCount++ {
			url := baseURL + strconv.Itoa(pageCount) // Sets the url used in http.Get()

			res, err := http.Get(url) // Fetches the commits on a page
			if err != nil {
				log.Println(http.StatusServiceUnavailable)
			}

			var commits []Commits                            // Struct slice to store commits on a page
			err = json.NewDecoder(res.Body).Decode(&commits) // Decoding data into Commits slice

			// If there is an error regarding a repository, number of commits is set to 0
			// and if not the amount of commits is set accordingly
			if err != nil {
				sourceAll[i].Commits = 0
			} else {
				sourceAll[i].Commits += len(commits)
			}

			res.Body.Close() // Closes body to prevent resource leak

			if len(commits) < 100 { // If commits on page is <100 then it's the last page
				whileBool = false // and whileBool is set to false
			}
		}
	}

	//						Sorting and displaying of repos
	//-----------------------------------------------------------------------------

	// Sorts repositories after commits in descending order
	sort.Slice(sourceAll, func(i, j int) bool {
		return sourceAll[i].Commits > sourceAll[j].Commits
	})

	var allRepos AllRepos // Struct that contains the finalized processed info

	for i := 0; i < limit; i++ {
		var tempRepo Repository
		tempRepo.Path = sourceAll[i].Repository
		tempRepo.Count = sourceAll[i].Commits
		allRepos.Repos = append(allRepos.Repos, tempRepo)
	}

	if token != "" { // Sets value of whether token was used or not
		allRepos.Auth = true
	}

	//							Checking webhooks
	//-----------------------------------------------------------------------------

	for i := 0; i < len(AllWebhooks); i++ { // Looping through all registered webhooks
		if AllWebhooks[i].Event == "commits" { // If their event is "commits", their url will be called
			var postPayload InvoWebhook              // Declaring a temp struct to store payload
			postPayload.Event = AllWebhooks[i].Event // Sets event
			postPayload.Time = AllWebhooks[i].Time   // Sets time of webhook creation
			postPayload.Params = "limit: " + limitString + "auth: " + strconv.FormatBool(allRepos.Auth)

			temp := new(bytes.Buffer) // Creates a temporary binary to store and encode json

			encoder := json.NewEncoder(temp) // Encodes into temp
			encoder.Encode(postPayload)      // Encodes the message

			_, err := http.Post(AllWebhooks[i].URL, "application/json", temp) // Posting payload to webhook url
			if err != nil {
				log.Println(http.StatusServiceUnavailable)
			}
		}
	}

	temp := new(bytes.Buffer) // Creates a temporary binary to store and encode json

	encoder := json.NewEncoder(temp) // Encodes into temp
	encoder.Encode(allRepos)         // Encodes the message

	w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) //	Prints to server
}
