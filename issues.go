package gitlabapi

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"
)

//									Structs
//-----------------------------------------------------------------------------

//Payload contains the data received from payload
type Payload struct {
	Project string `json:"project"`
}

//Issues stores the data about the issues and is used to find labels and user
type Issues struct {
	Labels []string `json:"labels"`
	Author User     `json:"author"`
}

//User contains the data about each user
type User struct {
	Username string `json:"username"`
	Count    int    `json:"count"`
}

//AllUsers contains the finalized user data that will be displayed
type AllUsers struct {
	Users []User `json:"users"`
	Auth  bool   `json:"auth"`
}

//Label contains the data about each label
type Label struct {
	Name  string `json:"label"`
	Count int    `json:"count"`
}

//AllLabels contains the finalized label data that will be displayed
type AllLabels struct {
	Labels []Label `json:"labels"`
	Auth   bool    `json:"auth"`
}

//HandlerIssues is the handler for the issues endpoint
func HandlerIssues(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") // Splits url into parts divided by '/'

	if len(parts) < 4 { // Checks if correct path was entered
		http.Error(w, "Bad url", http.StatusBadRequest)
		log.Println(http.StatusBadRequest)
		return
	}

	typeParam := r.FormValue("type") // Gets the type from url

	if typeParam != "users" && typeParam != "labels" { // Checks if valid type was used
		http.Error(w, "Bad type parameter", http.StatusBadRequest)
		log.Println(http.StatusBadRequest)
		return
	}

	token := r.FormValue("auth") //	Gets the auth token if used

	payload := Payload{} // Declaring an empty payload struct for decoding

	err := json.NewDecoder(r.Body).Decode(&payload) // Decoding payload into struct
	if err != nil {
		http.Error(w, "Payload processing failed", http.StatusNoContent)
		log.Println(http.StatusNoContent)
		return
	}

	sourceAll := getRepos(w, r) // Fetching the repositories

	//								Fetching issues
	//-----------------------------------------------------------------------------

	var projID int // Variable to store project id

	for i := 0; i < len(sourceAll); i++ { // Looping through repos until the first
		if sourceAll[i].Name == payload.Project { // repo with corresponding name is found
			projID = sourceAll[i].ID // Setting the project's ID to projID
		}
	}

	baseURL := GitlabRootPath + "/" + strconv.Itoa(projID) + "/issues?" // Base url for http.Get()

	if token != "" { // Adds token to url if used
		baseURL += "private_token=" + token + "&"
	}

	baseURL += "per_page=100&page=" // Adds paging to url

	var allIssues []Issues // delcaring struct slice to store all issues
	pageCount := 1         // Starting page count

	// Loops through all pages of issues until it hits a page with <100 issues
	for whileBool := true; whileBool; pageCount++ {
		url := baseURL + strconv.Itoa(pageCount) // Sets url for page to http.Get() from

		res, err := http.Get(url) // Fetching issues from url
		if err != nil {
			log.Println(http.StatusServiceUnavailable)
		}

		var pageIssues []Issues                             // Struct slice to store all issues on current page
		err = json.NewDecoder(res.Body).Decode(&pageIssues) // Decoding data into Commits slice
		if err != nil {
			log.Println(http.StatusNoContent)
		}

		if len(pageIssues) < 100 { // Updates whileBool if page has <100 issues
			whileBool = false
		}

		res.Body.Close() // Closing body to prevent resource leak

		for i := 0; i < len(pageIssues); i++ { // Appending all issues on current page to allIssues
			allIssues = append(allIssues, pageIssues[i])
		}
	}

	//							Fetching users/labels
	//-----------------------------------------------------------------------------

	temp := new(bytes.Buffer)        //	Creates a temporary binary to store and encode json
	encoder := json.NewEncoder(temp) // Encodes into temp
	var postPayloadBool bool         // Bool to send in post to show if token was used or not

	if typeParam == "users" { // If type=users all authors will be fetched from issues
		var allUsers AllUsers                 // Struct slice to store users
		for i := 0; i < len(allIssues); i++ { // Loops trhough all issues
			inSlice := false
			for j := 0; j < len(allUsers.Users); j++ { // Loops through all registered users
				if allUsers.Users[j].Username == allIssues[i].Author.Username { // If already registered, count is upped
					inSlice = true
					allUsers.Users[j].Count++
				}
			}
			if !inSlice { // If not registered, user is then registered and it's count is upped from 0
				var temp User
				temp.Username = allIssues[i].Author.Username
				temp.Count++
				allUsers.Users = append(allUsers.Users, temp)
			}
		}

		if token != "" { // Updates if token was used
			allUsers.Auth = true
			postPayloadBool = true
		}

		encoder.Encode(allUsers) // Encodes the message

	} else {
		var allLabels AllLabels               // Struct slice to store labels
		for i := 0; i < len(allIssues); i++ { // Loops through all issues
			inSlice := false
			for j := 0; j < len(allIssues[i].Labels); j++ { // Loops through all labels in issue
				for k := 0; k < len(allLabels.Labels); k++ { // Loops through all registered labels
					if allLabels.Labels[k].Name == allIssues[i].Labels[j] { // If already registered, count is upped
						inSlice = true
						allLabels.Labels[k].Count++
					}
				}
				if !inSlice { // If not registered, label is then registered and it's count is upped from 0
					var temp Label
					temp.Name = allIssues[i].Labels[j]
					temp.Count++
					allLabels.Labels = append(allLabels.Labels, temp)
				}
			}
		}

		if token != "" { // Updates if token was used
			allLabels.Auth = true
			postPayloadBool = true
		}

		encoder.Encode(allLabels) // Encodes the message
	}

	//							Checking webhooks
	//-----------------------------------------------------------------------------

	for i := 0; i < len(AllWebhooks); i++ { // Looping through all registered webhooks
		if AllWebhooks[i].Event == "issues" { // If their event is "issues", their url will be called
			var postPayload InvoWebhook              // Declaring a temp struct to store payload
			postPayload.Event = AllWebhooks[i].Event // Sets event
			postPayload.Time = AllWebhooks[i].Time   // Sets time of webhook creation
			postPayload.Params = "type: " + typeParam + ", project: " + payload.Project + ", auth: " + strconv.FormatBool(postPayloadBool)

			temp := new(bytes.Buffer) // Creates a temporary binary to store and encode json

			encoder := json.NewEncoder(temp) // Encodes into temp
			encoder.Encode(postPayload)      // Encodes the payload

			_, err := http.Post(AllWebhooks[i].URL, "application/json", temp) // Posting payload to webhook url
			if err != nil {
				log.Println(http.StatusServiceUnavailable)
			}
		}
	}

	w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) //	Prints to server
}
