package gitlabapi

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

//									Struct
//-----------------------------------------------------------------------------

//Status stores the data about the status
type Status struct {
	Gitlab   string `json:"gitlab"`
	Database string `json:"database"`
	Uptime   string `json:"uptime"`
	Version  string `json:"version"`
}

//								Handler for status
//-----------------------------------------------------------------------------

//HandlerStatus provides the status of the api
func HandlerStatus(w http.ResponseWriter, r *http.Request) {
	var stat Status //  Declares a diag struct element

	res, _ := http.Get(GitlabRootPath) //	Checking Gitlab status
	stat.Gitlab = res.Status           //  Sets status code for Gitlab
	res.Body.Close()                   //	Closes res to avoid resource leak

	// Checking database status by trying to get the first webhook
	_, err := Client.Collection(collection).Doc(AllWebhooks[0].id).Get(Ctx)
	var tempString string

	// Gets status of database by checking for errors
	if err == nil {
		tempString = "200 OK"
	} else {
		tempString = "503 Status service not found"
	}

	stat.Database = tempString //  Sets status code for firebase database

	stat.Version = Version                    //	Sets version
	stat.Uptime = time.Since(Uptime).String() //	Sets uptime

	//							Checking webhooks
	//-----------------------------------------------------------------------------

	for i := 0; i < len(AllWebhooks); i++ { // Looping through all registered webhooks
		if AllWebhooks[i].Event == "status" { // If their event is "status", their url will be called
			var postPayload InvoWebhook              // Declaring a temp struct to store payload
			postPayload.Event = AllWebhooks[i].Event // Sets event
			postPayload.Time = AllWebhooks[i].Time   // Sets time of webhook creation
			postPayload.Params = ""

			temp := new(bytes.Buffer) // Creates a temporary binary to store and encode json

			encoder := json.NewEncoder(temp) // Encodes into temp
			encoder.Encode(postPayload)      // Encodes the payload

			_, err := http.Post(AllWebhooks[i].URL, "application/json", temp) // Posting payload to webhook url
			if err != nil {
				log.Println(http.StatusServiceUnavailable)
			}
		}
	}

	temp := new(bytes.Buffer) //  Declares a byte array used as buffer

	encoder := json.NewEncoder(temp) //	Create an encoder with temp as destination
	encoder.Encode(stat)             //	Encodes stat

	w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
	w.WriteHeader(http.StatusOK)

	io.Copy(w, temp) // Prints to server
}
