# IMT2681 Cloud Technologies Assignment 2

This is a RESTful API developed in Golang 1.13, using the NTNU's deplyment of gitlab and Firebase. It's purpose is to be able to:

* Get a list of repositories ordered by number of commits in descending order. This can be done with and without access token.
* Get a list of all the authors that have created an issue, or all labels and how many times they've been used in a given project
* Get the status of gitlab and database
* POST, GET and DELETE webhooks

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Go version 1.13 installed
* A Google account
* Firebase dependencies and client libraries
* An access token to NTNU's deployment of gitlab

### Installing

To get the project up and running, you first have to clone it.

Then go to [firebase](https://console.firebase.google.com/) and create a new application. If you already have an application then just use it. In the application create a collection named "webhooks" and let it auto generate id's for documents. This is where the project will store data, but in order to let the project get access you need to get a .json file with private key.

You can get this file by following the steps regarding generating a private key file for your service account [here](https://firebase.google.com/docs/admin/setup#initialize_the_sdk). When you have the .json file with a private key, place it in the cloned repository in the same folder as README.md file and not in the "cmd" folder in the repository.

Now open the "extras.go" file and find the string constant "AdminSDK". Replace the current path with the path of your .json file. Your path should look like this: 
```
"./your-adminsdk-file-name-here.json" 
```
Save the "extras.go" file and close it, you should now be able to run the program. When running the program make sure you run it from the "cloudassignment2" folder and not the "cmd" folder within the "cloudassignment2" folder. Running it from the "cmd" folder will just result in an error. An example of how to run it if you cloned it to "Documents" folder would be:
```
user@laptop:~/Documents/cloudassignment2$ go run cmd/main.go
```
The project should now be up and running on your machine locally.

## Usage

The project has 4 endpoints, these are:
```
/repocheck/v1/commits
/repocheck/v1/issues
/repocheck/v1/customwebhook
/repocheck/v1/status
```

Example of running commits with multiple parameters:
```
/repocheck/v1/commits?limit=5&auth=<token>
```
Example of running commits with single parameter:
```
/repocheck/v1/commits?auth=<token>
```

Example of running issues with multiple parameters:
```
/repocheck/v1/issues?type=users&auth=<token>
```
Example of running issues with single parameter:
```
/repocheck/v1/issues?type=users
```

Example of running webhook to display all:
```
/repocheck/v1/customwebhook
```
Example of running webhook to display one:
```
/repocheck/v1/customwebhook/<webhook_id>
```

## Built with

* [NTNU's Gitlab API](https://docs.gitlab.com/ee/api/README.html)
* [Google's Firebase database](https://firebase.google.com/)

## Important points

I have implemented the following endpoints: commits, issues, status and webhooks. Thus I've opted not to implement languages endpoint.

If a repository's data can't be get, it's number of commits is set to 0.

In issues, the project with same name as the one sent in payload will be analyzed. However since these names aren't unique, I've chosen to only analyze the first match. I'm also considering "user" to be the author of the issue.

If user would send an invalid limit or no limit as parameter in commits, limit will then be set to default which is 5. This limit will also be sent by the webhooks as if it was the parameter the user used.

When creating a collection, it's name needs to be "webhooks" and it needs to let firebase auto generate id for documents.
