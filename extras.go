package gitlabapi

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
)

//									Constants
//-----------------------------------------------------------------------------

//Version contains the current version of api
const Version string = "v1"

//CommitsRootPath contains the root path for use of commits endpoint
const CommitsRootPath string = "/repocheck/" + Version + "/commits"

//IssuesRootPath contains the root path for use of issues endpoint
const IssuesRootPath string = "/repocheck/" + Version + "/issues"

//WebhooksRootPath1 contains the root path for use of webhooks endpoint
const WebhooksRootPath1 string = "/repocheck/" + Version + "/customwebhook"

//WebhooksRootPath2 contains the root path for use of webhooks endpoint
const WebhooksRootPath2 string = "/repocheck/" + Version + "/customwebhook/"

//StatusRootPath contains the root path for use of the status endpoint
const StatusRootPath string = "/repocheck/" + Version + "/status"

//GitlabRootPath contains the root path for use of gitlab api
const GitlabRootPath string = "https://git.gvk.idi.ntnu.no/api/v4/projects"

//AdminSDK contains the path for the file storing access token to database
const AdminSDK string = "./cloudassignment2-216db-firebase-adminsdk-6j3x7-b565f932e7.json"

//Collection is the name of the collection in firestore used by this program
const collection string = "webhooks"

//							Global variables
//-----------------------------------------------------------------------------

//Ctx is the context variable used when initializing databse in main
var Ctx context.Context

//Client is a pointer to the database client
var Client *firestore.Client

//Uptime is a declaration of a time object used to keep track of time
var Uptime time.Time

//									Structs
//-----------------------------------------------------------------------------

//RepoSource stores data fetched from gitlab about each repo
type RepoSource struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	Repository string `json:"path_with_namespace"`
	Commits    int    `json:"commits"`
}

//InvoWebhook contains the payload info sent to the webhook url
type InvoWebhook struct {
	Event  string `json:"event"`
	Time   string `json:"time"`
	Params string `json:"params"`
}

//								Helper functions
//-----------------------------------------------------------------------------

//GetRepos fetches all the repositories and returns them in a slice
func getRepos(w http.ResponseWriter, r *http.Request) []RepoSource {
	token := r.FormValue("auth") //	Gets the auth token if used

	var baseURL string // Declaring variable for the base url

	// Checks token, if empty then no token is used and if not empty token is added as parameter
	if token == "" {
		baseURL = GitlabRootPath + "?per_page=100&page="
	} else {
		baseURL = GitlabRootPath + "?per_page=100&private_token=" + token + "&page="
	}

	var sourceAll []RepoSource // Struct slice that contains all repos from all pages
	pageCount := 1             // Variable used to keep track of current page

	for whileBool := true; whileBool; pageCount++ { // Loops as long as whileBool = true
		var sourcePage []RepoSource              // Struct slice that contains all repos on a page
		url := baseURL + strconv.Itoa(pageCount) // Converts page num to string and concatenates it to url

		res, err := http.Get(url) // Gets all repositories on page
		if err != nil {
			log.Println(http.StatusServiceUnavailable)
		}

		err = json.NewDecoder(res.Body).Decode(&sourcePage) // Decodes repos into slice

		if len(sourcePage) < 100 { // Last page will always have <100 entries
			whileBool = false // Updates whileBool so loop will no longer run
		}

		res.Body.Close() // Closes body to prevent resource leak

		for i := 0; i < len(sourcePage); i++ { // Appends all repos from current page to slice of all repos
			sourceAll = append(sourceAll, sourcePage[i])
		}
	}

	return sourceAll // Returning the slice of all the repositories
}

//	delWebhook deletes the webhook "i" locally stored in AllWebhooks[]
func delWebhook(h []Webhook, i int) []Webhook {
	h[i] = h[len(h)-1]  // Sets index i to be equal to last index
	return h[:len(h)-1] // Returns slice that's 1 index smaller
}

//GetData fetches all documents from "webhooks" collection in database
func GetData() {
	iter := Client.Collection(collection).Documents(Ctx) // initialising iterator

	for { // Loops through all documents
		doc, err := iter.Next()   // Sets iterator to be next document
		if err == iterator.Done { // If iterator is done, then break free from loop
			break
		}
		if err != nil { // Logs error
			log.Println(err)
		}

		inSlice := false // Variable to keep track of whether webhook is stored locally or not
		var temp Webhook // Declaring a temp webhook struct object

		doc.DataTo(&temp) // Storing data from current document into temp

		temp.id = doc.Ref.ID // Setting auto generated id from firebase, so it can be stored locally

		// Looping through all webhooks
		for i := 0; i < len(AllWebhooks); i++ {
			if temp.id == AllWebhooks[i].id { // Checks if webhook is already stored locally
				inSlice = true
			}
		}
		if !inSlice { // if not stored locally, it's appended to AllWebhooks and thus stored locally
			AllWebhooks = append(AllWebhooks, temp)
		}
	}
}
