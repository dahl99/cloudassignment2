package gitlabapi

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

//Webhook contains data about webhooks
type Webhook struct {
	id    string `json:"id"`
	Event string `json:"event"`
	URL   string `json:"url"`
	Time  string `json:"time"`
}

//tempid used only to store the id of newly registered webhook so it can be displayed
type tempid struct {
	ID string `json:"id"`
}

//AllWebhooks contains all of the webhooks created
var AllWebhooks []Webhook

//HandlerWebhooks is the handler for the webhooks endpoint
func HandlerWebhooks(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		parts := strings.Split(r.URL.Path, "/") // Splits url into parts divided by '/'

		if len(parts) < 4 || len(parts) > 5 { // Checks if correct path was entered
			http.Error(w, "Bad url", http.StatusBadRequest)
			log.Println(http.StatusBadRequest)
			return
		}

		temp := new(bytes.Buffer)        // Creates a temporary binary to store and encode json
		encoder := json.NewEncoder(temp) // Encodes into temp

		if len(parts) == 4 { // If id wasn't sent in url
			encoder.Encode(AllWebhooks) // Encodes all the webhooks
		} else {
			var index int // Variable to store webhook index

			for i := 0; i < len(AllWebhooks); i++ { // Loops through all webhooks
				if AllWebhooks[i].id == parts[4] { // If webhook id matches id sent as parameter
					index = i // sets which index the webhook is in
				} else if i == len(AllWebhooks)-1 && AllWebhooks[i].id != parts[4] { // If webhook isn't found, a bad request is sent
					http.Error(w, "Webhook not found", http.StatusBadRequest)
					log.Println(http.StatusBadRequest)
					return
				}
			}

			encoder.Encode(AllWebhooks[index]) // Encodes the webhook with matching id
		}

		w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
		w.WriteHeader(http.StatusOK)

		io.Copy(w, temp) //	Prints to server

	case http.MethodPost:
		webhook := Webhook{} // Declaring an empty webhook struct object
		tempID := tempid{}   // Decalring an empty tempid struct object

		err := json.NewDecoder(r.Body).Decode(&webhook) // Decoding webhook into struct
		if err != nil {
			http.Error(w, "Payload decoding failed", http.StatusNoContent)
			log.Println(http.StatusNoContent)
			return
		}

		// If the event is invalid, a bad request is sent
		if webhook.Event != "commits" && webhook.Event != "issues" && webhook.Event != "status" {
			http.Error(w, "Invalid webhook event type", http.StatusBadRequest)
			log.Println(http.StatusBadRequest)
			return
		}

		webhook.Time = time.Now().String() // Sets time of creation

		// Adds webhook to firebase database which will assign auto id
		_, _, err = Client.Collection(collection).Add(Ctx, webhook)
		if err != nil {
			log.Println(err)
		}

		// Getting data from database so id can be assigned locally
		GetData()

		// Loops through all webhooks and using time variable to identify newly
		// created webhook and stores it's id in tempID object so only id will be displayed
		for i := 0; i < len(AllWebhooks); i++ {
			if AllWebhooks[i].Time == webhook.Time {
				tempID.ID = AllWebhooks[i].id
			}
		}

		temp := new(bytes.Buffer) // Creates a temporary binary to store and encode json

		encoder := json.NewEncoder(temp) // Encodes into temp
		encoder.Encode(tempID)           // Encodes the message

		w.Header().Add("Content-Type", "application/json") //	Displays the message formatted in json
		w.WriteHeader(http.StatusOK)

		io.Copy(w, temp) //	Prints to server

	case http.MethodDelete:
		parts := strings.Split(r.URL.Path, "/") // Splits url into parts divided by '/'

		if len(parts) < 4 || len(parts) > 5 { // Checks if correct path was entered
			http.Error(w, "Bad url", http.StatusBadRequest)
			log.Println(http.StatusBadRequest)
			return
		}

		var index int // Variable to store webhook index

		for i := 0; i < len(AllWebhooks); i++ { // Loops through all webhooks
			if AllWebhooks[i].id == parts[4] { // If webhook id matches id sent as parameter
				index = i // sets which index the webhook is in
			} else if i == len(AllWebhooks)-1 && AllWebhooks[i].id != parts[4] { // If webhook isn't found, a bad request is sent
				http.Error(w, "Webhook not found", http.StatusBadRequest)
				log.Println(http.StatusBadRequest)
				return
			}
		}

		// Deleting webhook document in firebase database
		_, err := Client.Collection(collection).Doc(AllWebhooks[index].id).Delete(Ctx)
		if err != nil {
			http.Error(w, "Webhook deletion failed", http.StatusServiceUnavailable)
			log.Println(http.StatusServiceUnavailable)
			return
		}

		// Deleting webhook locally
		AllWebhooks = delWebhook(AllWebhooks, index)

	default: // Sends error if invalid method
		http.Error(w, "Invalid method "+r.Method, http.StatusBadRequest)
	}
}
